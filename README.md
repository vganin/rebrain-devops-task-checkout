# rebrain-devops-task-checkout

This repository contains the default nginx configuration file for DevOps by rebrain Basics course

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

git

```
sudo apt install git 
```

### Installing

Clone this repo

```
git clone https://github.com/vganyn/rebrain-devops-task-checkout
```

PROFIT!

## Running the tests

I have no idea how to test it

## Deployment

There's nothing to deploy

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/vganyn/rebrain-devops-task-checkout/tags). 

## Authors

* **Viktor Ganin** - [vganyn](https://github.com/vganyn)
>
See also the list of [contributors](https://github.com/vganyn/rebrain-devops-task-checkout/graphs/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* **Billie Thompson** - [PurpleBooth](https://github.com/PurpleBooth) 

